from django.shortcuts import render
from .models import User, Schedule
from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView

# Create your views here.
def index(request):
    return render(request, 'index.html', {'nbar': 'index'})

def registration(request):
    response = {}
    return render(request, 'registration.html', {'nbar': 'register'})

def activity(request):
        ## IMPORTANT ##
    schedules = Schedule.objects.all()
    return render(request, 'activity.html', {'Schedule': schedules, 'nbar': 'activity'})

def add_user_form_submission(request):
    fullname = request.POST["fullname"]
    username = request.POST["username"]
    date = request.POST["birth_date"]
    email = request.POST["email"]
    password = request.POST["password"]
    pass_confirm = request.POST["password_confirm"]
    
    user_info = User(
        name=fullname, 
        date=date, 
        username = username,
        email = email,
        password = password,
        password_confirm = pass_confirm
    )
    user_info.save()
    return render(request, 'index.html', {})

def add_schedule_form_submission(request):
    title = request.POST["title"]
    category = request.POST["category"]
    place = request.POST["place"]
    date = request.POST["date"]
    time = request.POST["time"]
    
    schedule_info = Schedule(
        title = title, 
        category = category, 
        place = place,
        date = date,
        time = time
    )
    schedule_info.save()
    return render(request, 'activity.html', {})

def delete_schedules(request):
    schedules = Schedule.objects.all()
    schedules.delete()
    return render(request, 'activity.html')