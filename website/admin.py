from django.contrib import admin
from .models import User, Schedule

# Register your models here.
admin.site.register(User)
admin.site.register(Schedule)