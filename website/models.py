from django.db import models

# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=200)
    date = models.DateTimeField()
    username = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    password_confirm = models.CharField(max_length=200)
    
class Schedule(models.Model):
    title = models.CharField(max_length=200)
    category = models.CharField(max_length=100)
    place = models.CharField(max_length=200)
    date = models.DateField()
    time = models.TimeField()